#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<>
  Purpose: 
  Created: 08/16/2015
"""

import xml.etree.ElementTree as ET
from collections import defaultdict
import matplotlib.pyplot as plt
import optparse

def build_parser():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--input", dest="input", help="input")
    parser.add_option("-f", "--figure", dest="figure", help="port")
    parser.add_option("-n", "--name", dest="name", help="name")
    return parser

# Copyright: http://stackoverflow.com/questions/2290962/python-how-to-get-sorted-count-of-items-in-a-list
def leaders(xs):
    counts = defaultdict(int)
    for x in xs:
        counts[x] += 1
    return dict(sorted(counts.items(), reverse=True, key=lambda tup: tup[1]))


parser = build_parser()
options, _args = parser.parse_args()
if options.input:
    try:
            
        # Processing Data
        data_xml = ET.parse(options.input)
        root = data_xml.getroot()
        hosts = []
        uncommon_ports = {'27017': 'MongoDB',
                          '6379': 'Redis',
                          '11211': 'memcached',}
        
        
        for host in root.findall('host'):
            host_childs = host.getchildren()
            data = []
            for item in host_childs:
                host_state = None
                host_ipv4 = None        
                if item.tag == 'address':
                    if item.attrib['addrtype'] == 'ipv4':
                        host_ipv4 = item.attrib['addr']
                        data.append(host_ipv4)
                if item.tag == 'status':
                    host_state = item.attrib['state']
                    data.append(host_state)
                if item.tag == 'ports':
                    ports = item.getchildren()
                    for port in ports:
                        portid = port.attrib['portid']
                        port_child = port.getchildren()
                        port_name = None
                        port_state = None
                        for pc in port_child:
                            if pc.tag == 'state':
                                port_state = pc.attrib['state']
                            if pc.tag == 'service':
                                port_name = pc.attrib['name']
                        if None == port_name:
                            port_name = uncommon_ports.get(portid)
                        if port_state == 'open':                
                            data.append([port_name, portid, port_state])
            hosts.append(data)
           
        service_name = []
        total_host = 0
        for host in hosts:
            if len(host) > 2:        
                total_host += 1
                ip, ports = host[1], host[2:]
                for port in ports:
                    service_name.append(port[0])
                    
        
        _tmp = leaders(service_name)
        labels = _tmp.keys()
        sizes = _tmp.values()
        plt.pie(sizes, labels=labels,
                autopct='%1.1f%%', shadow=True, startangle=90)
        plt.axis('equal')
        plt.suptitle('%s, total %s servers' % (options.name, total_host), size = 16)
        plt.savefig(options.figure)
    except:
        print 'Error parsing xml'
