#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<>
  Purpose: 
  Created: 08/16/2015
"""

import subprocess
import optparse

def build_parser():
    parser = optparse.OptionParser()
    parser.add_option("-p", "--port", dest="port", help="port", default = '27017,21,3306,6379,11211')
    parser.add_option("-i", "--ip", dest="ip", help="ip", default = '127.0.0.1')
    parser.add_option("-o", "--output", dest="output", help="output", default = 'data.xml')
    return parser

parser = build_parser()
options, _args = parser.parse_args()
print 'Scanning %s, %s. Data will be saved on %s' % (options.port, options.ip, options.output)
command = "nmap -T5 --host-timeout 4s --min-rate 1000 -n -p%s  %s -oX %s" % (options.port, options.ip, options.output)
proc = subprocess.Popen(command, stdout = subprocess.PIPE, stderr = subprocess.PIPE, shell= True)
#Blocking
proc.communicate()